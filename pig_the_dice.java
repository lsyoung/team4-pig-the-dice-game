import java.io.*;
import java.util.*;

public class pig_the_dice {
    static Player[] player;
    static int N;

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
	  System.out.println("플레이어 수를 입력하세요(2 ~ 10) (-1을 입력하면 종료)");

        while (N < 2 || N > 10) {
            System.out.print("플레이어 수: ");
            N = Integer.parseInt(br.readLine());
            if (!validatePlayerNum(N)) continue;

            if (N == -1) {
                System.out.println("게임을 종료합니다.");
                break;
            }
        }

        player = new Player[N];

        for (int i = 0; i < N; i++)
            player[i] = new Player(i + 1, 0);

        for (int i = 0; i < N; i++) {
            System.out.println("플레이어 " + (i + 1) + " 게임을 시작합니다.");
            getScore(i);
        }

        for (int i = 0; i < N; i++)
            System.out.println("player " + (i + 1) + ": " + player[i].score + "점");

        Arrays.sort(player, (o1, o2) -> (int) (o2.score - o1.score));

        System.out.println("=> 우승자는 플레이어 " + player[0].idx + " (" + player[0].score + "점) 입니다.");
    }


    static class Player {
        int idx;
        long score;

        public Player(int idx, long score) {
            this.idx = idx;
            this.score = score;
        }
    }
}
